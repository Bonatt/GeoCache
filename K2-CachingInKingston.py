import numpy as np
from collections import defaultdict
import operator
from collections import Counter


cipher = str(np.genfromtxt('K2-CachingInKingston.txt', dtype='str', delimiter='#')) # Delimiter "#" because not in txt.

'''
I 6DA BIBC1 VA ZD11A'H BI4TDH. 8VD 4DHY 21IP YC1 1AYG21 P1HB2GTYGVA YV S1Y INN YC1 GA6V24IYGVA 8VD WGNN A11P YV 6GAP YCGH BIBC1. YCGH BIBC1 BVAYIGA12 WIH GAHTG21P 98 2 S8THG1H WGYC AV 4VHH 91YW11A VD2 YV1H, 'BIBCGAS GA Y2D2V' (SB18W61). YC121 I21 IN21IP8 HV41 BIBC1H GA YCGH HY8N1 GA EGASHYVA, 9DY YCGH VA1 CIH I HNGSCYN8 IPP1P YWGHY. YC1 BVAYIGA12 GH I 21SDNI2 4GB2V BVAYIGA12, CGPP1A GA 9VYY121NN CINN. YC1 9IH141AY V6 YC1 9DGNPGAS BVAYIGAH YC1 92IBE1A C1INYC HBG1AB1H NG92I28, I NG92I28 V6 9VVEH IAP YC1 BIBC1 GH CGPGAS I4VASHY YC14. 8VD WGNN 91 21ZDG21P YV 1AY12 YC1 9DGNPGAS GA V2P12 YV 4IE1 YCGH 6GAP. 8VD 4DHY 91 21HT1BY6DN V6 YC1 HYDP1AYH HYDP8GAS GA YC1 9DGNPGAS. TN1IH1 91 ZDG1Y IAP IBY IBBV2PGASN8. YC1 CVD2H V6 VT12IYGVA BIA 91 6VDAP IY NG92I28 PVY ZD11AHD PVY BI HNIHC C1INYC HNIHC CVD2H (WGYC AV WWW GA 62VAY). WC1A 8VD 1AY12 9VYY121NN'H 62VAY 1AY2IAB1 YC1 NG92I28 GH YV 8VD2 2GSCY. 1AY12 YC1 NG92I28 IAP 4IE1 8VD2 WI8 YV YC1 9IH141AY DHGAS YC1 HYIG2H YV 8VD2 N16Y. WC1A 8VD S1Y YV YC1 9IH141AY GY GH AVW DT YV 8VD YV 6GAP YC1 BIBC1. TN1IH1 21TNIB1 YC1 BIBC1 1MIBYN8 IH 6VDAP. GY GH AVY GA YC1 BV4TDY12 H8HY14. TN1IH1 PVA'Y BIDH1 HDHTGBGVA 62V4 YC1 T1VTN1 I2VDAP 8VD; GY 4I8 91 I SVVP GP1I YV YIE1 YC1 BIBC1 YV I HDGYI9N1 NVBIYGVA YV HGSA YC1 NVS, HV YC1 BIBC1 NVBIYGVA PV1HA'Y S1Y HDHTGBGVDH B2VWPH I2VDAP GY. G6 8VD I21 6GAPGAS YCGH BIBC1 VA YC1 PI8 V6 E YWV, TN1IH1 91 IWI21 YCIY 4IA8 BIBC12H WGNN 91 BV4GAS YC2VDSC VA YCGH PI8, IAP GY GH IHE1P YCIY 8VD 1AY12 GA H4INN S2VDTH IAP YIE1 YD2AH, 91 21HT1BY6DN IAP ZDG1Y. YC1 BIBC1 GH NI91NN1P IH WF YWV YWV PVY H1F1A W P1BG4IN 1GSCY8 1GSCY VA1 YC211, YWV YCVDHIAP IAP AGA1. TN1IH1 CIF1 6DA WGYC YCGH BIBC1 IAP YCIAEH YV YC1 S8THG1H 6V2 YC1 GP1I V6 YC1 BIBC1 BVAYIGA12!
'''
print ''
print cipher


### 1:1 frequency of character in cipher to most frequent letter in English.
# Remove symbols that will mess with frequency calculation
deciphered = cipher

symbols = [' ', '!', '\'', '(', ')', '.', ',', ';']
for i in symbols: 
  cipher = cipher.replace(i,'')
  
# Reparse into individial symbols
chars = list(cipher)

# Sorted dictionary by freq.
chars2 = Counter(chars)

# Sorted list by freq.
#chars3 = [i for i in chars2] # Doesn't really sort... sorts alphanumerically
#chars3 = sorted(chars2.items(), key=operator.itemgetter(1))
chars3 = [chars2.most_common()[i][0] for i in range(len(chars2))]

# Most frequent letters in English, most to least.
# Various resources:
freq = ['e','t','a','o','i','n','s','h','r','d','l','c','u','m','w','f','g','y','p','b','v','k','j','x','q','z']
freq = ['e','t','a','o','i','n','s','r','h','l','d','c','u','m','f','p','g','w','y','b','v','k','x','j','q','z']
freq = ['e','t','a','o','i','n','s','r','h','d','l','u','c','m','f','y','w','g','p','b','v','k','x','q','j','z']
# My own mixing after reading results, swapping two letters at a time and finding words that I knew.
freq = ['e','t','a','o','n','i','s','h','r','c','u','l','d','y','b','g','w','f','m','p','k','q','v','x','j','z']

# Clip last two to match len(chars3) (24)
freq2 = freq[:-2]

print ''
print chars3
print freq2
# Change letters
for i in range(len(freq2)):
  deciphered = deciphered.replace(chars3[i], freq2[i])

print ''
print deciphered





'''
a fun cache on queen's campus. you must read the entire description to get all the information you will need to find this cache. this cache container was inspired by r gypsies with no moss between our toes, 'caching in truro' (gceywfe). there are already some caches in this style in kingston, but this one has a slightly added twist. the container is a regular micro container, hidden in botterell hall. the basement of the building contains the bracken health sciences library, a library of books and the cache is hiding amongst them. you will be required to enter the building in order to make this find. you must be respectful of the students studying in the building. please be quiet and act accordingly. the hours of operation can be found at library dot queensu dot ca slash health slash hours (with no www in front). when you enter botterell's front entrance the library is to your right. enter the library and make your way to the basement using the stairs to your left. when you get to the basement it is now up to you to find the cache. please replace the cache exactly as found. it is not in the computer system. please don't cause suspicion from the people around you; it may be a good idea to take the cache to a suitable location to sign the log, so the cache location doesn't get suspicious crowds around it. if you are finding this cache on the day of k two, please be aware that many cachers will be coming through on this day, and it is asked that you enter in small groups and take turns, be respectful and quiet. the cache is labelled as wv two two dot seven w decimal eighty eight one three, two thousand and nine. please have fun with this cache and thanks to the gypsies for the idea of the cache container!

- Microcontainer in Botterell Hall.
- Basement, Bracken Health Sciences Library.
- Open hours NOT at library.queensu.ca/health/hours
- Front entrance, go right.
- Go to basement via stairs on left.
- Take cache away [to table] to sign it.
- "wv two two dot seven w decimal eighty eight one three, two thousand and nine"
- WV22.7W.8813 2009
'''


